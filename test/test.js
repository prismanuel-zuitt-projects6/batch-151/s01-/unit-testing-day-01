const { assert } = require('chai')
const { newUser } = require('../index.js')
//describe and it is from Mocha
describe('Test newUser object', () => {
	it('Assert newUser type is object', () => {
		assert.equal(typeof(newUser), 'object')
	});
	it('Assert newUser.email is type string', () => {
		assert.equal(typeof(newUser.email), 'string')
	});
	it('Assert newUser.email is not undefined', () => {
		assert.notEqual(typeof(newUser.email), 'undefined')
	});
	it('Assert that newUser.password type is string', () => {
		assert.equal(typeof(newUser.password), 'string')
	});
	it('Assert newUser.password is not undefined', () => {
		assert.notEqual(typeof(newUser.password), 'undefined')
	});
	it('Assert that password length is 16 characters long', () => {
		assert.isAtLeast(newUser.password.length,16)
	});
	it('Assert that the newUser firstName type is a string', () => {
		assert.equal(typeof(newUser.firstName), 'string')
	});
	it('Assert that the newUser lastName type is a string', () => {
		assert.equal(typeof(newUser.lastName), 'string')
	});
	it('Assert newUser firstName is not undefined', () => {
		assert.notEqual(typeof(newUser.firstName), 'undefined')
	});
	it('Assert newUser lastName is not undefined', () => {
		assert.notEqual(typeof(newUser.lastName), 'undefined')
	});
	it('Assert newUser age is at least 18', () => {
		assert.isAtLeast(newUser.age, 18)
	});
	it('Assert that the newUser age type is a number', () => {
		assert.equal(typeof(newUser.age), 'number')
	});
	it('Assert that the newUser contact number is a string', () => {
		assert.equal(typeof(newUser.contactNumber), 'string')
	});
	it('Assert that the newUser batch number is a number', () => {
		assert.equal(typeof(newUser.batchNumber), 'number')
	});
	it('Assert newUser batchNumber is not undefined', () => {
		assert.notEqual(typeof(newUser.batchNumber), 'undefined')
	});

});