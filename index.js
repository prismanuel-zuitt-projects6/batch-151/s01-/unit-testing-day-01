const express = require('express')
const app = express()
const port = 5000

app.use(express.json());

// app.get('/', (req, res) => {
//   res.send('Hello World!')
// })
const newUser = {
  firstName: 'John',
  lastName: 'Dela Cruz',
  age: 18,
  contactNumber: '09123456789',
  batchNumber: 151,
  email: 'john.delacruz@gmail.com',
  password: 'sixteencharacters'
}

module.exports = {
  newUser: newUser
}

app.listen(port, () => {
  console.log(`Server is running on port ${port}`)
})